from django.contrib import admin

# Register your models here.
from patrick_site.models import Guest, Contacts

admin.site.register(Guest)
admin.site.register(Contacts)