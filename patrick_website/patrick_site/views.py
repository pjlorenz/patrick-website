from django.contrib.auth import login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader

from .forms import GuestForm, ContactForm, SearchForm, EditForm, DeleteForm
from .models import Guest, Contacts


def home(request):
    template = loader.get_template('home.html')
    context = {'employees': Guest.objects.all()}
    return HttpResponse(template.render(context, request))


def thanks(request):
    template = loader.get_template('thanks.html')
    context = {}
    return HttpResponse(template.render(context, request))


def resume(request):
    template = loader.get_template('resume.html')
    context = {}
    return HttpResponse(template.render(context, request))


def signup(request):
    if request.method == 'POST':
        form = GuestForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/thanks/')
    else:
        form = GuestForm()
    return render(request, 'signup.html', {'form': form, 'guests': Guest.objects.all()})

def contacts(request, contact_id=None):
    # this statement refers to both searching and adding a new contact
    if request.method == 'POST':
        if 'searched' not in request.POST: # nothing entered into search bar, which means we're adding a new contact
            contacts = Contacts.objects.all()
            form = ContactForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/contacts/')
        else:
            searched = request.POST['searched']
            form = ContactForm()
            contacts = Contacts.objects.filter(last_name__contains=searched)

    else:
        form = ContactForm()  # todo: add edit functionality
        contacts = Contacts.objects.all()

    return render(request, 'contacts.html', {'form': form, 'contacts': contacts})

def search(request):
    if request.method == 'POST':
        searched = request.POST['searched']
        contacts = Contacts.objects.filter(last_name__contains=searched)
        return render(request, 'contacts.html', {'searched': searched, 'contacts': contacts})

    else:
        form = SearchForm()
    return render(request, 'contacts.html')


def edit(request, contact_id=None):
    # if statement to determine whether you're editing a contact or not
    if request.method == 'POST':
        # if you are editing a contact then this form variable holds the instance
        # of the contact that you clicked edit for.
        form = ContactForm(request.POST, instance=Contacts.objects.get(pk=contact_id))
        # this checks to see if the form is valid and saves it to the database
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/contacts')  # returns you to the contacts page
    else:
        # if not editing contact, then this returns the edit page with contact filled in
        form = ContactForm(instance=Contacts.objects.get(pk=contact_id))
        # this form also contains the instance you clicked on
        # if you click 'edit contact' then it will take you back to contacts page
    return render(request, 'edit.html', {'form': form})


def contact_edit(request, pk=None):
    contact = get_object_or_404(Contacts, pk=pk)
    if request.method == "POST":
        form = ContactForm(request.POST, instance=contact)
        if form.is_valid():
            form.save()
            return redirect('contacts')
    else:
        form = ContactForm(instance=contact)
    return render(request, 'edit.html', {'form': form, 'contact': contact})


def delete(request, pk=None):

    if request.method == 'POST':
        contact = get_object_or_404(Contacts, pk=pk)
        contact.delete()
        return HttpResponseRedirect('/contacts')
    else:
        form = ContactForm()

    return render(request, 'delete.html', {'form': form})
