import form as form
from django import forms
from django.forms import ModelForm

from patrick_site.models import Guest, Contacts


class GuestForm(ModelForm):
    class Meta:
        model = Guest
        fields = ['first_name', 'last_name', 'email', 'comment']


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contacts
        fields = ['first_name', 'last_name', 'email', 'address', 'zip_code', 'phone']

        widgets = { 'first_name': forms.TextInput(attrs={'class': 'form-control'}),
                    'last_name': forms.TextInput(attrs={'class': 'form-control'}),
                    'email': forms.TextInput(attrs={'class': 'form-control'}),
                    'address': forms.TextInput(attrs={'class': 'form-control'}),
                    'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
                    'phone': forms.TextInput(attrs={'class': 'form-control'}),
                   }

class SearchForm(forms.Form):
    class Meta:
        model = Contacts
        fields = ['first_name', 'last_name', 'email', 'address', 'zip_code', 'phone']

class EditForm(forms.Form):
    class Meta:
        model = Contacts
        fields = ['first_name', 'last_name', 'email', 'address', 'zip_code', 'phone']


class DeleteForm(ModelForm):
    class Meta:
        model = Contacts
        fields = '__all__'
