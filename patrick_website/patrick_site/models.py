from django.db import models


class Guest(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, null=True)
    comment = models.TextField(max_length=400, null=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.first_name

class Contacts(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, null=True)
    address = models.CharField(max_length=300)
    zip_code = models.CharField(max_length=5)
    phone = models.CharField(max_length=10)

    def __str__(self):
        return self.first_name
