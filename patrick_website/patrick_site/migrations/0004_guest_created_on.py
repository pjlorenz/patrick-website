# Generated by Django 3.2 on 2021-04-23 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patrick_site', '0003_alter_guest_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='guest',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
