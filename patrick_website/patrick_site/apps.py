from django.apps import AppConfig


class PatrickSiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'patrick_site'
